/* 
 * Aeden McClain (c) 2016-2018
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

#include "platypro.base/common.h"
#include "platypro.base/net.h"

#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>

bool Server_Host_P(SERVERNODE* node, char* port)
{
  bool result = false;
  int sockfd = -1;
  struct sockaddr_in server_addr;

  sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if(sockfd >= -1)
  {
    int optval = 1;
    int optlen = sizeof(optval);
   
    fcntl(sockfd, F_SETFL, O_NONBLOCK);
    setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &optval, optlen);
    
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = INADDR_ANY;
    server_addr.sin_port = htons(atoi(port));

    if(bind(sockfd, (struct sockaddr*) &server_addr, sizeof(struct sockaddr_in))>=0)
    {
        listen(sockfd, 5);

        node->addr = server_addr.sin_addr;
        node->fd   = sockfd;
        result = true;
    } else
    {
      Log_ThrowFormat(node->errormanager, ERRSEV_USERWARN, ERRID_CONNECTION, strerror(errno));
    }
  }
  return result;
}

bool Server_Connect_P(SERVERNODE* node, char* address, char* port)
{
  int result = false;
  int sockfd = -1;

  struct addrinfo info;
  struct addrinfo* newinfo;

  memset(&info, 0, sizeof(struct addrinfo));

  //Load our static options
  info.ai_family = AF_UNSPEC;
  info.ai_socktype = SOCK_STREAM;
  info.ai_protocol = IPPROTO_TCP;

  if(!getaddrinfo(address, port, &info, &newinfo))
  {
    sockfd = socket(newinfo->ai_family, newinfo->ai_socktype,
          newinfo->ai_protocol);
    if(sockfd >= -1)
    {
      int optval = 1;
      int optlen = sizeof(optval);
      if(setsockopt(sockfd, SOL_SOCKET, SO_KEEPALIVE, &optval, optlen) == 0) 
      {
        int err = connect(sockfd,newinfo->ai_addr, (int)newinfo->ai_addrlen);

        if (err >= 0)
        {
          //Turn off blocking
          fcntl(sockfd, F_SETFL, O_NONBLOCK);
          //Update Server Connection
          node->fd = sockfd;
          inet_aton(newinfo->ai_addr->sa_data, &node->addr);
          *(node->sendBuffer) = '\n';
          result = true;
        } 
        else
        {
          Log_ThrowFormat(node->errormanager, ERRSEV_USERWARN, ERRID_CONNECTION, strerror(errno));
        }
      }
    }
  }
  return result; 
}

bool Server_Accept_P(SERVERNODE* node, SERVERNODE* server)
{
  bool result = false;
  struct sockaddr_in addr;
  socklen_t resultsize = sizeof(struct sockaddr_in);
  int fd = accept(server->fd, 
               (struct sockaddr *) &addr, 
               &resultsize);
  if(fd > (-1))
  {
    //Turn off blocking
    fcntl(fd, F_SETFL, O_NONBLOCK);

    node->fd = fd;
    node->addr = addr.sin_addr;

    Log_ThrowFormat(node->errormanager, ERRSEV_INFO, STR_CONNECTION_NEW, inet_ntoa(addr.sin_addr));
    result = true;
  } 

  return result;
}
