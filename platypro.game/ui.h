/*
 * Aeden McClain (c) 2016-2018
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef H_PLM_GAME_UI
#define H_PLM_GAME_UI

/** \file ui.h

    UI Engine.

 *  \defgroup platypro_game_ui UI Engine.
 *  \ingroup platypro_game
 */
/** @{ */

#include "platypro.game/tools.h"
#include "platypro.base/list.h"

struct UI_Element;
struct UI_Index;

//! Generic Event function for UI
typedef int (*UI_EVENT) (struct UI_Index*, struct UI_Element*, void* state);

//Maximum string length for text
#define MAX_STRING_LEN 64

typedef enum UI_Type
{
 UITYPE_BTN,
 UITYPE_TEXTBOX,
 UITYPE_LABEL,
 UITYPE_CHECK
} UI_TYPE;

typedef enum UI_HoverState
{
  UIHOVER_DEFAULT,
  UIHOVER_ROLLOVER,
  UIHOVER_PRESS
} UI_HOVERSTATE;

typedef struct UI_Style
{
  IO_COLOR buttonColor;
  IO_COLOR textboxColor;
  IO_COLOR labelColor;
  IO_COLOR hoverColor;
  IO_COLOR pressColor;
  IO_COLOR focusedColor;
} UI_STYLE;

typedef struct UI_Index
{
  int indexAt;

  //Text cache, for refilling empty textboxes
  char textcache[MAX_STRING_LEN];
  
  LIBSTATE* state;
  UI_STYLE* style;
  IO_FONT font;
  LIST uiObjects;
  struct UI_Element* focus; //!< The currently focused element
} UI_INDEX;

typedef struct UI_Def
{
  char* name;
  UI_TYPE type;
  char* text;
  IO_RECT dimens;

  UI_EVENT event;
} UI_DEF;

typedef struct UI_Element
{
  int index;
  char name[MAX_STRING_LEN];

  UI_HOVERSTATE hoverState;
  UI_INDEX* parentIndex;

  char text[MAX_STRING_LEN];
  IO_RECT dimens;

  UI_EVENT event;
} UI_ELEMENT;

#define UI_TRUE "T"
#define UI_FALSE "F"

#define CHECKBOX_PADDING 10

#define UIFONTSIZE 37.0f

/*! Creates a new UI state
 *  /param libs   The LibHelper State to draw objects with
 *  /param style  Defines UI Styling. Leave NULL to use default style
 */
extern UI_INDEX* UI_Create(LIBSTATE* libs, UI_STYLE* style, IO_FONT font);

//! Cleans up a UI state
extern void UI_Destroy(UI_INDEX**);

//! Updates all UI Elements (Input states, Events)
extern bool UI_Update(UI_INDEX*, EVENTSTATE e, void* state);

//! Draws the current UI state
extern bool UI_Draw(UI_INDEX*);

//
// UI Creation Functions
//

//! Create an object
extern int UI_Object_Create(UI_INDEX* ui, struct UI_Def object);

//! Create multiple objects
void UI_Object_Auto(UI_INDEX* ui, UI_DEF* uiElements, size_t uiElementsSize, LIST stringfile);

//! Destroy an object
extern void UI_Object_Destroy(UI_INDEX* ui, int object);

//! Destroys all objects
extern void UI_Object_DestroyAll(UI_INDEX* ui);

//! Get the text of an object
extern UI_ELEMENT* UI_Object_Get(UI_INDEX* ui, int object);

//! Gets a UI element by name
extern UI_ELEMENT* UI_Object_GetByName(UI_INDEX* ui, char* name);

//! Focus on the specified object
extern bool UI_Focus(UI_INDEX* ui, UI_ELEMENT* object);

//! Sets object text
extern bool UI_Object_SetText(UI_INDEX* ui, int object, char* text);

/** @} */
#endif /* SRC_CLIENT_UI_H_ */
