/*
 * Aeden McClain (c) 2016-2018
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef H_PLM_GAME_VIEW
#define H_PLM_GAME_VIEW

/** \file view.h

    View Manager.

 *  \defgroup platypro_game_view View Manager
 *  \ingroup platypro_game
 */
/** @{ */

#include "platypro.base/list.h"
#include "platypro.game/tools.h"

typedef char* (*VIEW_INIT)   (void* global, void* local);
typedef char* (*VIEW_UPDATE) (void* global, void* local, EVENTSTATE* e);
typedef char* (*VIEW_DRAW)   (void* global, void* local);
typedef char* (*VIEW_CLEAN)  (void* global, void* local);

typedef struct GameView
{
  char* viewName;
  void*     data; //!< Stores view data
  size_t dataSize;

  VIEW_INIT   init;
  VIEW_UPDATE update;
  VIEW_DRAW   draw;
  VIEW_CLEAN  clean;
} GAMEVIEW;

typedef struct ViewManager
{
  GAMEVIEW*   current; //!< The active view
  GAMEVIEW*   next;    //!< The next active view

  void* global;        //!< User Global Data

  GAMEVIEW* floatview; //!< A floating view

  LIST viewIndex;      //!< An index of all views
  bool doBreak;        //!< A flag for whether to quit the view loop
} VIEWMANAGER;

//! Creates a new viewmanager
extern VIEWMANAGER* Views_Init(void* global);

//! Destroys a viewmanager
extern void         Views_Cleanup(VIEWMANAGER** views);

/*! Creates a new view.
 *  \param viewName  Name to identify the view in future
 *  \param dataSize  Size of generated structure private to view
 *  \param init      Init function for view
 *  \param update    Update function for view
 *  \param draw      Draw function for view
 *  \param clean     Cleanup function for view
 *  \returns Success
 */
extern bool Views_Create(VIEWMANAGER* views,
    char* viewName,
    size_t dataSize,
    VIEW_INIT   init,
    VIEW_UPDATE update,
    VIEW_DRAW   draw,
    VIEW_CLEAN  clean);
;
//! Sets the current view
extern void*        Views_Set(VIEWMANAGER* vm, char* name);

//! Gets the active view's user structure
extern void*        Views_Get(VIEWMANAGER* vm);

//! Sets the current floating view
extern void*        Views_SetFloat(VIEWMANAGER* vm, char* name);

//! Closes the current floating view
extern bool         Views_CloseFloat(VIEWMANAGER* vm);

//! Removes a view from the manager
extern bool         Views_Remove(VIEWMANAGER* vm, char* name);

#define VIEW_CONTINUE "__VIEWDOCONTINUE" //!< Continue to drawing
#define VIEW_BREAK "__VIEWDOBREAK"    //!< Don't draw

//! Update all views
extern bool Views_Update (LIBSTATE* ls, EVENTSTATE* e, void* manager);

//! Draw all views
extern bool Views_Draw   (LIBSTATE* ls, void* manager);

/** @} */
#endif
