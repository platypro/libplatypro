/* 
 * Aeden McClain (c) 2016-2018
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

#include "platypro.base/common.h"
#include "platypro.game/multiplayer.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>

#include <platypro.base/list.h>
#include <platypro.base/net.h>

#define TYPE_MIN 5

#define TYPE_LIST   0
#define TYPE_MESG   1
#define TYPE_PDEAD  2

PLAYERINDEX* Player_Init
	(size_t playerSize, 
	 int signalcount, 
	 PLAYERGEN dataGen, 
	 PLAYERGET dataRecv, 
	 PLAYERUPDATE dataUp, 
	 void* user)
{
  PLAYERINDEX* result = calloc(1, sizeof(PLAYERINDEX));
  if(result)
  {
    result->idCount++;
    result->playerSize = playerSize;
    result->signalCount = signalcount;
    result->dataGen    = dataGen;
    result->dataRecv   = dataRecv;
    result->dataUpdate = dataUp;
	result->userContext = user;
  }
  return result;
}

PRIVATE bool taskIndex_Clean(void* object, unsigned int key, void* data)
{
  if(key == TYPE_LIST || key == TYPE_MESG)
  {
    free(object);
  }
  return true;
}

void Player_Clean(PLAYERINDEX** index)
{
  PLAYERINDEX* i = *index;
  //Request a connection close to all clients
  Player_Order(*index, ORDER_SERVERCLOSE);
  
  //Wait for connections to close
  while(i->playerCount > 0)
  {
    Player_Update(*index, 0); 
  }
  
  //Clear player data
  if(i->clients)
  {
    List_Purge(&i->clients, NULL);
  }
  
  //Clear taskIndex
  if(i->taskIndex)
  {
    List_Purge(&i->taskIndex, taskIndex_Clean);
  }
  
  //Clear Chat
  if(i->taskIndex_Signal)
  {
    List_Purge(&i->taskIndex_Signal, NULL);
  }
    
  free(*index);
}

PUBLIC void Player_Order(PLAYERINDEX* index, char order)
{
  if(index)
  {
    CLIENT* p;
    foreach(p, index->clients)
    {
      p->sendTask_Order = order;
    }
  }
  
  return;
}

TASK_REF Player_PushList(PLAYERINDEX* index, char header, LIST list, SVRGEN format)
{
  CLIENTLIST* clist = List_Add_Alloc(&index->taskIndex, sizeof(CLIENTLIST), TYPE_LIST);
  if(clist)
  {
    clist->header   = header;
    clist->list     = list;
    clist->callback = format;
  }
  
  return (TASK_REF)clist;
}

PRIVATE void Player_Deref(PLAYERINDEX* index, TASK_REF obj)
{
  //Dereference all players with TASK_REF
  CLIENT* c;
  foreach(c, index->clients)
  {
    if(c->sendTask && LIST_GETOBJ(c->sendTask) == obj)
    {
      c->sendTask = c->sendTask->sibling;
    }
  }
}

bool Player_Pop(PLAYERINDEX* index, TASK_REF ref)
{
  if(ref)
  {
    Player_Deref(index, ref);
    List_Remove(&index->taskIndex, ref);
    return true;
  } else return false;
}

PRIVATE TASK_REF Player_PushSignal(LIST* list, char header, char* mesg)
{
  CLIENTMESSAGE* cmesg = List_Add_Alloc(list, sizeof(CLIENTMESSAGE), TYPE_MESG);
  if(mesg)
  {
    cmesg->header = header;
    strncpy(cmesg->mesg, mesg, MAX_MESSAGE_SIZE);
  }
  
  return (TASK_REF)mesg;
}

TASK_REF Player_PushMesg(PLAYERINDEX* index, char header, char* mesg) {
  return Player_PushSignal(&index->taskIndex, header, mesg);
}

void Player_PushOnce(PLAYERINDEX* index, char header, char* format, ...)
{
  static char buffer[MAX_MESSAGE_SIZE]; 
  
  va_list args;
  va_start (args, format);
  vsnprintf(buffer, MAX_MESSAGE_SIZE, format, args);
  va_end(args);
  
  Player_PushSignal(&index->taskIndex_Signal, header, buffer);
  
  //Update client hooks
  if(index->clients)
  {
    LIST lastChat = List_FindLast(index->taskIndex_Signal);
    CLIENT* c;
    foreach(c, index->clients)
    {
      if(!c->sendTask_Signal)
      {
        c->sendTask_Signal = lastChat;
      }
    }
  }
  return;
}

PUBLIC void Player_SendAll(PLAYERINDEX* index, char skipstatic)
{
  LIST newIndex = index->taskIndex;
  if(index)
  {
    //Skip static tasks at beginning of list
    if(skipstatic)
    {
      while(
        LIST_GETTYPE(newIndex) == TYPE_MESG ||
        LIST_GETTYPE(newIndex) == TYPE_LIST)
      {
        newIndex = LIST_GETNEXT(newIndex);
      }
    }
    
    CLIENT* p;
    foreach(p, index->clients)
    {
      p->sendTask = newIndex;
    }
  }
  return;
}

PRIVATE bool Player_SetLast(LIST clients, LIST tasks)
{
  CLIENT* d;
  //Distribute signals
  foreach(d, clients)
  {
    if(d->sendTask == NULL)
    {
      d->sendTask = List_FindLast(tasks);
    }
  }
  return true; 
}

PUBLIC bool Player_Do(PLAYERINDEX* index, PLAYERDO pdo)
{
  if(index->clients)
  {
    CLIENT* c;
    foreach(c, index->clients)
    {
      if(!pdo(c, index->userContext))
        return false;
    }
  }
  return true;
}

PUBLIC bool Player_Distribute(PLAYERINDEX* context, LISTTYPE tasktype)
{
  LIST newList = List_Promote(&context->taskIndex, context->currentClient, tasktype + TYPE_MIN);
  
  //Update Client pointers into ClientData
  CLIENT* cl;
  foreach(cl, context->clients)
  {
    if(cl->sendTask == NULL)
    {
      cl->sendTask = newList;
    } 
    else if(cl->sendTask == newList)
    {
      cl->sendTask = context->taskIndex;
    }
  }
  
  return true;
}

void* Player_Add(PLAYERINDEX* index, SERVERNODE* node, char order, int* pid)
{
  void* result = NULL;
  if(index->playerCount >= SERVER_MAX_PLAYERS)
  {
    char buff[2] = {ORDER_SERVERFULL, '\000'};
    //Server is full, kick the player
    Server_PutMesg(&node, SIGNAL_ORDER, buff);
  } 
  else 
  {
    CLIENT* c = List_Add_Alloc(&index->clients, sizeof(CLIENT), TYPE_NONE);
    if(c)
    {
      result = calloc(1, index->playerSize);
      if(result)
      {
        static char buffer[MAX_MESSAGE_SIZE];
        
        int i;
        for(i = 0; i< index->signalCount; i++)
        {     
          //Add user signals
          List_Add(&index->taskIndex, c, TYPE_MIN + i);
          
          if(i == 0)
          {
            Player_SetLast(index->clients, index->taskIndex);
          }
        }
       
        c->playerData      = result;
        c->sendTask        = index->taskIndex; 
        c->sendTask_Signal = index->taskIndex_Signal;
        c->sendTask_Order  = order;
        c->id              = index->idCount;
        c->node            = node;
        
        *pid = c->id;
        
        //Tell the client their ID
        snprintf(buffer, 10, FORMAT_PLYRID, index->idCount);
        Server_PutMesg(&node, SIGNAL_PLYRID, buffer);
        
        index->idCount++;
        index->playerCount++;
      }
    }
    else Player_Clean(&index);
  }
  return result;
}

void Player_Remove(PLAYERINDEX* index, CLIENT** c)
{
  //Close Server Node
  Server_Close(&(*c)->node);
  
  (*c)->dead++;
  
  index->dataGen(index, NULL, 0);
  
  free((*c)->playerData);

  if(index->clients)
  {
    //Distribute a player death message
    List_Add(&index->taskIndex, (*c), TYPE_PDEAD);
    Player_SetLast(index->clients, index->taskIndex);
  }
  
  index->playerCount --;
  return;
}

PRIVATE void clearDeadPlayer(PLAYERINDEX* index, CLIENT** playerObj)
{
  if((*playerObj)->dead > index->playerCount)
  {
    int remres = true;

    //Get rid of task blocks
    Player_Deref(index, (TASK_REF)*playerObj);
    while(remres) remres = List_Remove(&index->taskIndex, *playerObj);

    //Remove player from master list
    List_Remove(&index->clients, *playerObj);
    free(*playerObj);
    *playerObj = NULL;
  }
}

PRIVATE int Player_SendDead(PLAYERINDEX* index, CLIENT* c)
{
  CLIENT* playerObj = LIST_GETOBJ(c->sendTask);
  char buffer[MAX_MESSAGE_SIZE];
  snprintf(buffer, MAX_MESSAGE_SIZE, "%d",
    playerObj->id
  );

  playerObj->dead++;

   return Server_PutMesg(&c->node, SIGNAL_PLAYERDEAD, buffer);
}

PRIVATE int Player_SendPlayer(PLAYERINDEX* index, CLIENT* c)
{
  CLIENT* playerObj = LIST_GETOBJ(c->sendTask);
  int result = MSG_SUCCESS;
  char buffer[MAX_MESSAGE_SIZE];

  if(playerObj->node)
  {
    char h = index->dataGen(
	  index,
      buffer, 
      LIST_GETTYPE(c->sendTask) - TYPE_MIN
    );
    
    if(h != SIGNAL_NODATA)
      result = Server_PutMesg(&c->node, h, buffer);
  }

  return result;
}

int Player_SendSignal(PLAYERINDEX* index, CLIENT* c)
{
  CLIENTMESSAGE* mesg = LIST_GETOBJ(c->sendTask_Signal);
  
  char buffer[MAX_MESSAGE_SIZE + 2];
  snprintf(buffer, MAX_MESSAGE_SIZE + 2, FORMAT_SIGNAL, mesg->mesg);
  
  int result = Server_PutMesg(&c->node, mesg->header, buffer);

  if(result >= 0)
  {
    c->sendTask_Signal = c->sendTask_Signal->sibling;
  }
  
  return result;
}

int Player_SendOrder(PLAYERINDEX* index, CLIENT* c)
{
  char buff[2] = {c->sendTask_Order, '\000'};
  int result = Server_PutMesg(&c->node, SIGNAL_ORDER, buff);

  switch(c->sendTask_Order)
  {
    case ORDER_KICK: case ORDER_SERVERCLOSE: case ORDER_SERVERFULL:
      Player_Remove(index, &c);
    default:
      break;
  }
  
  if(c) c->sendTask_Order = ORDER_NONE;
  
  return result;
}

int pgetServer(char header, char* request, int listID, void* data)
{
  PLAYERINDEX* index = (PLAYERINDEX*)data;
  if (header == SIGNAL_KILLREQUEST)
  {
    index->currentClient->sendTask_Order = ORDER_KICK;
  }
  
  index->dataRecv(index, header, request);
  return true;
}

int pdoUpdate(void* object, unsigned int key, void* data)
{
  PLAYERINDEX* index = (PLAYERINDEX*)data;
  CLIENT* c = (CLIENT*)object;
  
  //Clear dead players (Which everyone knows about)
  clearDeadPlayer(index, &c);
  
  if(c && c->node && c->dead == 0)
  {
	index->currentClient = c;
    
    index->dataUpdate(index, index->ticks);
    if(c->sendTask_Signal == index->taskIndex_Signal)
    {
      index->numSignal++;
    }
    
    //Get player updates
    Server_Get(c->node, pgetServer, index);
    
    //Test heartbeat
    c->tick += index->ticks;
    
    if(c->tick > 1000)
    {
      c->tick = 0;
      Server_Heartbeat(&c->node);
    }

    int writeStatus = Server_Write(&c->node);
    
    if(c->node)
    {
      //Push player updates 
      if(Server_CheckBusy(c->node))
      {          
        if(c->sendTask_Order)
          writeStatus = Player_SendOrder(index, c);
        else if(c->sendTask)
        {
          LISTTYPE lt = LIST_GETTYPE(c->sendTask);
          
          if(lt == TYPE_LIST)
          {
            CLIENTLIST* cl = (CLIENTLIST*)LIST_GETOBJ(c->sendTask);
            writeStatus = Server_PutList(&c->node, cl->list, cl->callback, cl->header, c);
          }
          else if(lt == TYPE_MESG)
          {
            CLIENTMESSAGE* cm = (CLIENTMESSAGE*)LIST_GETOBJ(c->sendTask);
            if(c->node) writeStatus = Server_PutMesg(&c->node, cm->header, cm->mesg);
          }
          else if(lt == TYPE_PDEAD)
            writeStatus = Player_SendDead(index, c);
          //Pass type to user
          else writeStatus = Player_SendPlayer(index, c);
          
          if(writeStatus >= 0)
            c->sendTask = c->sendTask->sibling;
        } 
        else if(c->sendTask_Signal)
          writeStatus = Player_SendSignal(index, c);
      }
    }
    
    if(writeStatus == MSG_NOCONN)
    {
      Player_Remove(index, &c);
      return false;
    }
  }
  return true;
}

bool Player_Update(PLAYERINDEX* index, int ticks)
{
  if(index->clients)
  {
    index->numSignal = 0;
    //Update ticks
    index->ticks = ticks;

    List_Map(&index->clients,pdoUpdate,(void*)index);
    
    if(index->playerCount == 0)
    {
      List_Clear(&index->clients); 
    }
    
    if(index->numSignal == 0 && index->taskIndex_Signal)
    {
      char* signal = LIST_GETOBJ(index->taskIndex_Signal);
      List_Remove(&index->taskIndex_Signal, signal);
      free(signal);
    }
  }
  return true;
}
