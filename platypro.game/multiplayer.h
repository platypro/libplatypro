/* 
 * Aeden McClain (c) 2016-2018
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

#ifndef H_PLM_GAME_NET
#define H_PLM_GAME_NET

/** \file net.h

    This file is responsible for defining
    the server-side multiplayer network API.

 *  \defgroup platypro_game_net Multiplayer
 *  \ingroup platypro_game
 */
/** @{ */

#include <platypro.base/net.h>

#define foreachplayer(player, index) foreach(player, index->clients)
#define resetplayer(player, index) player->sendTask = index->taskIndex

typedef int SERVERORDER;
struct Client;
struct PlayerIndex;

#define ORDER_NONE 0
#define ORDER_KICK 1
#define ORDER_SERVERCLOSE 2
#define ORDER_SERVERFULL 3

#define ORDER_(num) num + 5

#define SIGNAL_ORDER       SIGNAL_(0,4)  //!< Give the client an order
#define SIGNAL_PLYRID      SIGNAL_(0,5)  //!< "This is your player id", sent before all other signals
#define SIGNAL_PLAYERDEAD  SIGNAL_(0,6)  //!< A request to remove player from player list
#define SIGNAL_KILLREQUEST SIGNAL_(0,7)  //!< Request a disconnect

#define FORMAT_SIGNAL "%s\t"
#define FORMAT_PLYRID "%d" //Player ID

//! Gets the currently processed client from PLAYERINDEX*
#define MP_THIS(cx) (cx)->currentClient

//! Gets the client to generate signals for
#define MP_OTHER(cx) MP_THIS(cx)->sendTask ? (CLIENT*)LIST_GETOBJ(MP_THIS(cx)->sendTask) : NULL

//! Gets the user data passed into Player_Init for this PLAYERINDEX*
#define MP_EXTRA(cx) (cx)->userContext

//! Gets the player structure from client
#define MP_CLIENT(client) (client)->playerData

//! Gets the id from client
#define MP_ID(client) (client)->id

typedef void* TASK_REF;

/** \brief A function to be called for each player when Player_Do is called
 *  \param client    Player structure
 *  \param user      User data passed into Player_Init
 *  \returns Success
 */
typedef int  (*PLAYERDO)     (struct Client* client, void* user);

/** \brief A function to be called on every call to Player_Update for each player.
 *  \param cx        PLAYERINDEX for this callback, can be indexed with MP_* functions
 *  \param tick      Ticks since last call to this function
 *  \returns Success
 */
typedef int  (*PLAYERUPDATE) (struct PlayerIndex* cx, int tick);

/** \brief A function for generating signals
 *  \param cx        PLAYERINDEX for this callback, can be indexed with MP_* functions
 *  \param buffer    Buffer of size MAX_MESSAGE_SIZE to be filled with an outgoing message, NULL if player just died.
 *  \param signal    The task type
 *  \returns The signal type which the outgoing. If zero then the signal is not to be sent
 */
typedef char (*PLAYERGEN)    (struct PlayerIndex* cx, char* buffer, int type);

/** \brief A function for processing incoming signals
 *  \param cx        PLAYERINDEX for this callback, can be indexed with MP_* functions
 *  \param header    Request header
 *  \param request   Request body
 *  \returns The signal type which the outgoing signal will have. If zero then the signal is not to be sent
 */
typedef int  (*PLAYERGET)    (struct PlayerIndex* cx, char header, char* request);

typedef struct ClientMessage
{
  char header;
  char mesg[MAX_MESSAGE_SIZE];
} CLIENTMESSAGE;

typedef struct ClientList
{
  char header;
  SVRGEN callback;
  LIST list;
} CLIENTLIST;

//! Definition of all required data for a single client
typedef struct Client
{
  //! The clients connection data to the server
  SERVERNODE* node;
  
  //! Current position in taskIndex
  LIST sendTask;
  
  //! Current position in taskIndex_Signal
  LIST sendTask_Signal;
  
  //! Pending order to send this player
  SERVERORDER sendTask_Order;
  
  //! Unique ID number. No two players get the same one
  int id;

  //! Number of ticks since last heartbeat
  int tick;
  
  //! Funeral invitation count
  int dead;

  //! User data for player
  void* playerData;
} CLIENT;

typedef struct PlayerIndex
{
  PLAYERGEN    dataGen;
  PLAYERGET    dataRecv;
  PLAYERUPDATE dataUpdate;
  
  //! List of client objects. see struct Client
  LIST clients;
  
  //! List of teams
  LIST teams;
  
  /*! A list with each element representing a signal, 
   *  or group of signals to be sent to all players.
   * 
   * This list is kept in order as new signals arrive.
   * When a player arrives in-game, this list is
   * populated with data for every signal that the
   * player can generate.
   * 
   * General data can be added to this list which should 
   * be sent to all players.
   * 
   * This list is cleared when the game is closed, and
   * player elements are removed when a client disconnects.
   */
  LIST taskIndex;
  
  /*! A list of signals which need to be destroyed after
   *  being sent to all clients
   * 
   * This list is kept seperate to ease garbage collection
   */
  LIST taskIndex_Signal;
  
  /*! The total number of user signals.
   * 
   * This number represents the total number of tasks added to
   * taskIndex whenever a new player joins. The user can pass
   * values from 0 to (signalCount - 1) to Player_Send which
   * requests that a user signal needs to be sent.
   *
   * The number specified to Player_Send is later passed into 
   * the server generation callback along with the player data
   * itself.
   */
  int signalCount;
  
  //! Next ID to give out
  unsigned int idCount;
  
  size_t playerSize;
  
  int playerCount;

  CLIENT* currentClient; //!< Current client being processed
  void* userContext; //!< User context passed into Player_Init
  
  //! Used in Player_Update
  int numSignal;
  int ticks;
} PLAYERINDEX;

#define PLAYER_GETCOUNT(player) (player)->playerCount

#define SERVER_MAX_PLAYERS 45

/** \brief Initializes multiplayer
 *  \param playerSize   Size of player structure
 *  \param signalcount  Number of task blocks to generate for each player
 *  \param dataGen      Function to generate signals from tasks
 *  \param dataRecv     Function to handle any signals recieved
 *  \param update       Function to be called per player, per call to Player_Update
 *  \param user         User pointer to pass to all callbacks
 *  \returns An index of players to pass into future functions.
 */
extern PLAYERINDEX* Player_Init(
  size_t playerSize, int signalcount, PLAYERGEN dataGen, PLAYERGET dataRecv, PLAYERUPDATE update, void* user);

/** \brief Destroys a multiplayer index
 *  \param index        Player index to destroy
 */
extern void Player_Clean(PLAYERINDEX** index);

/** \brief Adds a new player to the index
 *  \param index        Player index to add
 *  \param node         Server node to add to the index
 *  \param order        Initial order to send the player
 *  \param pid          Reference to an int to store the ID of the new player
 *  \returns The user structure for the new player.
 */
extern void* Player_Add(PLAYERINDEX* index, SERVERNODE* node, char order, int* pid);

/** \brief Resends all tasks.
 *  \param index        Player index to use
 *  \param skipstatic   Skips stale task blocks not generated by "Player_PushList" or "Player_PushMesg"
 */
extern void Player_SendAll(PLAYERINDEX* index, char skipstatic);

/** \brief Pushes a list to the task store
 *  \param index        Player index to use
 *  \param header       Signal header
 *  \param list         List to push
 *  \param format       The function to generate signals based off of the list items
 *  \returns A reference type which can be popped using Player_Pop
 */
extern TASK_REF Player_PushList(PLAYERINDEX* index, char header, LIST list, SVRGEN format);

/** \brief Pushes a single message to the task store
 *  \param index        Player index to use
 *  \param header       Signal Header
 *  \param mesg         Message to push
 *  \returns A reference type which can be popped using Player_Pop
 */
extern TASK_REF Player_PushMesg(PLAYERINDEX* index, char header, char* mesg);

/** \brief Pops a list from the task store
 *  \param index        Player index to use
 *  \param obj          Object to pop
 *  \returns Success
 */
extern bool Player_Pop(PLAYERINDEX* index, TASK_REF obj);

/** \brief Executes a function for each player
 *  \param index        Player index to use
 *  \param pdo          Function to call for each player
 *  \returns False if any of the function calls failed
 */
extern bool Player_Do(PLAYERINDEX* index, PLAYERDO pdo);

/** \brief Sends an order to all clients. 
  This function will overturn any pending orders.
 *  \param index        Player index to use
 *  \param order        Order to set
 */
extern void Player_Order(PLAYERINDEX* index, char order);

/** \brief Pushes a signal to all players.
  The variable args are passed into vsnprintf internally over the format parameter
 *  \param index        Player index to use
 *  \param header       Signal Header
 *  \param format       Format to be passed into vsnprintf
 */
extern void Player_PushOnce(PLAYERINDEX* index, char header, char* format, ...);

/** \brief Process updates for all players in index
 *  \param index        Player index to use
 *  \param ticks        Number of ticks to process
 */
extern bool Player_Update(PLAYERINDEX* index, int ticks);

/** Trigger a task from the task store
 *  \param index        struct PlayerContext* passed into user function
 *  \param taskType     Task to send
 */
extern bool Player_Distribute(PLAYERINDEX* index, LISTTYPE tasktype);

/** @} */
#endif
